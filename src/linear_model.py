"""Simple model to regress 3d human poses from 2d joint locations"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.python.ops import variable_scope as vs

import os
import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import data_utils
import cameras as cam
import copy
from keras.layers import Input, LSTM, TimeDistributed, Lambda, Reshape, SimpleRNN, Bidirectional
from keras.layers.core import Flatten, Dense, Dropout, Activation, RepeatVector
from keras.layers.merge import Concatenate
from keras import initializers, regularizers
import keras.backend as K


def my_init(shape, dtype=None):
	return K.random_normal(shape, stddev=0.01, dtype=dtype)


def kaiming(shape, dtype, partition_info=None):
	"""Kaiming initialization as described in https://arxiv.org/pdf/1502.01852.pdf
	Args
		shape: dimensions of the tf array to initialize
		dtype: data type of the array
		partition_info: (Optional) info about how the variable is partitioned.
			See https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/init_ops.py#L26
			Needed to be used as an initializer.
	Returns
		Tensorflow array with initial weights
	"""
	return tf.truncated_normal(shape, dtype=dtype) * tf.sqrt(2 / float(shape[0]))


class PoseJointMerge(Concatenate):
	def _merge_function(self, inputs):
		rhip = (inputs[0][:,3:6] + inputs[5][:,6:9] + inputs[7][:,15:18]) / 3.0
		rhip = K.reshape(rhip, (-1, 3))
		rknee = (inputs[0][:,6:9] + inputs[5][:,3:6] + inputs[7][:,18:21]) / 3.0
		rknee = K.reshape(rknee, (-1, 3))
		rfoot = (inputs[0][:,9:12] + inputs[5][:,0:3] + inputs[7][:,21:24]) / 3.0
		rfoot = K.reshape(rfoot, (-1, 3))
		lhip = (inputs[1][:,3:6] + inputs[5][:,12:15] + inputs[8][:,15:18]) / 3.0
		lhip = K.reshape(lhip, (-1, 3))
		lknee = (inputs[1][:,6:9] + inputs[5][:,15:18] + inputs[8][:,18:21]) / 3.0
		lknee = K.reshape(lknee, (-1, 3))
		lfoot = (inputs[1][:,9:12] + inputs[5][:,18:21] + inputs[8][:,21:24]) / 3.0
		lfoot = K.reshape(lfoot, (-1, 3))
		spine = (inputs[0][:,0:3] + inputs[1][:,0:3] + inputs[4][:,0:3] + inputs[5][:,9:12] + inputs[7][:,12:15] + inputs[8][:,12:15]) / 6.0
		spine = K.reshape(spine, (-1, 3))
		thorax = (inputs[2][:,0:3] + inputs[3][:,0:3] + inputs[4][:,3:6] + inputs[6][:,9:12] + inputs[7][:,9:12] + inputs[8][:,9:12]) / 6.0
		thorax = K.reshape(thorax, (-1, 3))
		neck_head = inputs[4][:,6:]
		lshoulder = (inputs[3][:,3:6] + inputs[6][:,12:15] + inputs[7][:,6:9]) / 3.0
		lshoulder = K.reshape(lshoulder, (-1, 3))
		lelbow = (inputs[3][:,6:9] + inputs[6][:,15:18] + inputs[7][:,3:6]) / 3.0
		lelbow = K.reshape(lelbow, (-1, 3))
		lwrist = (inputs[3][:,9:12] + inputs[6][:,18:21] + inputs[7][:,0:3]) / 3.0
		lwrist = K.reshape(lwrist, (-1, 3))
		rshoulder = (inputs[2][:,3:6] + inputs[6][:,6:9] + inputs[8][:,6:9]) / 3.0
		rshoulder = K.reshape(rshoulder, (-1, 3))
		relbow = (inputs[2][:,6:9] + inputs[6][:,3:6] + inputs[8][:,3:6]) / 3.0
		relbow = K.reshape(relbow, (-1, 3))
		rwrist = (inputs[2][:,9:12] + inputs[6][:,0:3] + inputs[8][:,0:3]) / 3.0
		rwrist = K.reshape(rwrist, (-1, 3))
		all_joints = K.concatenate((rhip, rknee, rfoot, lhip, lknee, lfoot, spine, thorax, neck_head, lshoulder, lelbow, lwrist, rshoulder, relbow, rwrist), axis=1)

		# spine = (inputs[0][:, 0:3] + inputs[1][:, 0:3] + inputs[4][:, 0:3]) / 3.0
		# spine = K.reshape(spine, (-1, 3))
		# thorax = (inputs[2][:, 0:3] + inputs[3][:, 0:3] + inputs[4][:, 3:6]) / 3.0
		# thorax = K.reshape(thorax, (-1, 3))
		# RS_RE_RA = inputs[2][:, 3:]
		# LS_LE_LA = inputs[3][:, 3:]
		# RH_RK_RF = inputs[0][:, 3:]
		# LH_LK_LF = inputs[1][:, 3:]
		# NK_HEAD = inputs[4][:, 6:]
		# all_joints = K.concatenate((RH_RK_RF, LH_LK_LF, spine, thorax, NK_HEAD, LS_LE_LA, RS_RE_RA), axis=1)

		return all_joints

	def compute_output_shape(self, input_shape):
		shape1 = list(input_shape[0])
		shape1.pop()
		shape1.append(16 * 3)
		return tuple(shape1)


def getRightLeg(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [6, 0, 1, 2])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1


def getLeftLeg(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [6, 3, 4, 5])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1


def getRightArm(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [7, 13, 14, 15])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1


def getLeftArm(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [7, 10, 11, 12])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1


def getHeadSpine(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [6, 7, 8, 9])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1

def getUpperLimb(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [2, 1, 0, 6, 3, 4, 5])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1

def getLowerLimb(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [15, 14, 13, 7, 10, 11, 12])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1

def getLeftArmRightLeg(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [12, 11, 10, 7, 6, 0, 1, 2])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1

def getRightArmLeftLeg(x):
	x1 = K.permute_dimensions(x, (1, 2, 0))
	x1 = K.gather(x1, [15, 14, 13, 7, 6, 3, 4, 5])
	x1 = K.permute_dimensions(x1, (2, 0, 1))
	return x1

class LinearModel(object):
	""" A simple Linear+RELU model """

	def __init__(self, linear_size, num_layers, residual, batch_norm, max_norm, batch_size, learning_rate, summaries_dir,
				 predict_14=False, is_training=True, is_predict=False, add_grammar=False, dtype=tf.float32):
		"""Creates the linear + relu model
		Args
			linear_size: integer. number of units in each layer of the model
			num_layers: integer. number of bilinear blocks in the model
			residual: boolean. Whether to add residual connections
			batch_norm: boolean. Whether to use batch normalization
			max_norm: boolean. Whether to clip weights to a norm of 1
			batch_size: integer. The size of the batches used during training
			learning_rate: float. Learning rate to start with
			summaries_dir: String. Directory where to log progress
			predict_14: boolean. Whether to predict 14 instead of 17 joints
			dtype: the data type to use to store internal variables
		"""

		# There are in total 17 joints in H3.6M and 16 in MPII (and therefore in stacked
		# hourglass detections). We settled with 16 joints in 2d just to make models
		# compatible (e.g. you can train on ground truth 2d and test on SH detections).
		# This does not seem to have an effect on prediction performance.
		self.HUMAN_2D_SIZE = 16 * 2

		# In 3d all the predictions are zero-centered around the root (hip) joint, so
		# we actually predict only 16 joints. The error is still computed over 17 joints,
		# because if one uses, e.g. Procrustes alignment, there is still error in the
		# hip to account for!
		# There is also an option to predict only 14 joints, which makes our results
		# directly comparable to those in https://arxiv.org/pdf/1611.09010.pdf
		self.HUMAN_3D_SIZE = 14 * 3 if predict_14 else 16 * 3

		self.input_size = self.HUMAN_2D_SIZE
		self.output_size = self.HUMAN_3D_SIZE

		self.isTraining = tf.placeholder(tf.bool, name="isTrainingflag")
		self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

		# Summary writers for train and test runs
		self.train_writer = tf.summary.FileWriter(os.path.join(summaries_dir, 'train'))
		self.test_writer = tf.summary.FileWriter(os.path.join(summaries_dir, 'test'))

		self.linear_size = linear_size
		self.batch_size = batch_size
		self.learning_rate = tf.Variable(float(learning_rate), trainable=False, dtype=dtype, name="learning_rate")
		self.global_step = tf.Variable(0, trainable=False, name="global_step")
		decay_steps = 100000  # empirical
		decay_rate = 0.96  # empirical
		self.learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, decay_steps, decay_rate)
		self.queue_encoder_inputs = None
		self.queue_decoder_outputs = None
		self.queue_decoder_outputs_joint = None

		# === Transform the inputs ===
		with vs.variable_scope("inputs"):

			# in=2d poses, out=3d poses
			enc_in = tf.placeholder(dtype, shape=[None, self.input_size], name="enc_in")
			if is_training:
				dec_out = tf.placeholder(dtype, shape=[None, self.output_size], name="dec_out")
				dec_out_joint = tf.placeholder(dtype, shape=[None, self.output_size], name="dec_out_joint")
			elif is_predict:
				dec_out = tf.placeholder(dtype, shape=[None, self.input_size], name="fake")
				dec_out_joint = tf.placeholder(dtype, shape=[None, self.input_size], name="fake_joint")
			else:
				dec_out = tf.placeholder(dtype, shape=[None, self.output_size], name="fake")
				dec_out_joint = tf.placeholder(dtype, shape=[None, self.output_size], name="fake_joint")
			self.encoder_inputs = enc_in
			self.decoder_outputs = dec_out
			self.decoder_outputs_joint = dec_out_joint

		# === Create the linear + relu combos ===
		with vs.variable_scope("linear_model"):

			# === First layer, brings dimensionality up to linear_size ===
			w1 = tf.get_variable(name="w1", initializer=kaiming, shape=[self.HUMAN_2D_SIZE, linear_size], dtype=dtype,
								 trainable=True)
			b1 = tf.get_variable(name="b1", initializer=kaiming, shape=[linear_size], dtype=dtype, trainable=True)
			w1 = tf.clip_by_norm(w1, 1) if max_norm else w1
			y3 = tf.matmul(enc_in, w1) + b1

			if batch_norm:
				y3 = tf.layers.batch_normalization(y3, training=self.isTraining, name="batch_normalization")
			y3 = tf.nn.relu(y3)
			y3 = tf.nn.dropout(y3, self.dropout_keep_prob)

			# === Create multiple bi-linear layers ===
			y31 = self.two_linear(y3, linear_size, residual, self.dropout_keep_prob, max_norm, batch_norm, dtype, 0)
			y32 = self.two_linear(y31, linear_size, residual, self.dropout_keep_prob, max_norm, batch_norm, dtype, 1)

			# === Intermediate linear layer has HUMAN_3D_SIZE in output ===
			w4_joint = tf.get_variable(name="w4_joint", initializer=kaiming, shape=[linear_size, self.HUMAN_3D_SIZE],
									   dtype=dtype, trainable=True)
			b4_joint = tf.get_variable(name="b4_joint", initializer=kaiming, shape=[self.HUMAN_3D_SIZE], dtype=dtype,
									   trainable=True)
			w4_joint = tf.clip_by_norm(w4_joint, 1) if max_norm else w4_joint
			y4_joint = tf.matmul(y3, w4_joint) + b4_joint

			w4t_joint = tf.get_variable(name="w4t_joint", initializer=kaiming, shape=[self.HUMAN_3D_SIZE, linear_size],
										dtype=dtype, trainable=True)
			b4t_joint = tf.get_variable(name="b4t_joint", initializer=kaiming, shape=[linear_size], dtype=dtype,
										trainable=True)
			w4t_joint = tf.clip_by_norm(w4t_joint, 1) if max_norm else w4t_joint
			y4t_joint = tf.matmul(y4_joint, w4t_joint) + b4t_joint

			y_up = y3 + y32 + y4t_joint
			y31r = self.two_linear(y_up, linear_size, residual, self.dropout_keep_prob, max_norm, batch_norm, dtype, 3)
			y32r = self.two_linear(y31r, linear_size, residual, self.dropout_keep_prob, max_norm, batch_norm, dtype, 4)

			# === Last linear layer has HUMAN_3D_SIZE in output ===
			w4r = tf.get_variable(name="w4r", initializer=kaiming, shape=[linear_size, self.HUMAN_3D_SIZE], dtype=dtype,
								  trainable=True)
			b4r = tf.get_variable(name="b4r", initializer=kaiming, shape=[self.HUMAN_3D_SIZE], dtype=dtype,
								  trainable=True)
			w4r = tf.clip_by_norm(w4r, 1) if max_norm else w4r
			y4r = tf.matmul(y32r, w4r) + b4r

			w4r_joint = tf.get_variable(name="w4r_joint", initializer=kaiming, shape=[linear_size, self.HUMAN_3D_SIZE],
										dtype=dtype, trainable=True)
			b4r_joint = tf.get_variable(name="b4r_joint", initializer=kaiming, shape=[self.HUMAN_3D_SIZE], dtype=dtype,
										trainable=True)
			w4r_joint = tf.clip_by_norm(w4r_joint, 1) if max_norm else w4r_joint
			y4r_joint = tf.matmul(y32r, w4r_joint) + b4r_joint

			# === End linear model ===
			if add_grammar:
				y = self.add_grammar(y4r_joint)

		# Store the outputs here
		self.outputs = y4r
		if add_grammar:
			self.outputs_joint = y
		else:
			self.outputs_joint = y4r_joint
		if is_training:
			if add_grammar:
				self.loss = tf.reduce_mean(tf.square(y4r - dec_out)) + tf.reduce_mean(
					tf.square(y4r_joint - dec_out_joint)) + tf.reduce_mean(
					tf.square(y4_joint - dec_out_joint)) + tf.reduce_mean(tf.square(y - dec_out_joint))
			else:
				self.loss = tf.reduce_mean(tf.square(y4r - dec_out)) + tf.reduce_mean(
					tf.square(y4r_joint - dec_out_joint)) + tf.reduce_mean(tf.square(y4_joint - dec_out_joint))
			self.loss_summary = tf.summary.scalar('loss/loss', self.loss)

			# To keep track of the loss in mm
			self.err_mm = tf.placeholder(tf.float32, name="error_mm")
			self.err_mm_summary = tf.summary.scalar("loss/error_mm", self.err_mm)

			# Gradients and update operation for training the model.
			opt = tf.train.AdamOptimizer(self.learning_rate)
			update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

			with tf.control_dependencies(update_ops):

				# Update all the trainable parameters
				gradients = opt.compute_gradients(self.loss)
				self.gradients = [[] if i == None else i for i in gradients]
				self.updates = opt.apply_gradients(gradients, global_step=self.global_step)

		# Keep track of the learning rate
		self.learning_rate_summary = tf.summary.scalar('learning_rate/learning_rate', self.learning_rate)

		# To save the model
		self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=None)

	def two_linear(self, xin, linear_size, residual, dropout_keep_prob, max_norm, batch_norm, dtype, idx):
		"""
		Make a bi-linear block with optional residual connection
		Args
			xin: the batch that enters the block
			linear_size: integer. The size of the linear units
			residual: boolean. Whether to add a residual connection
			dropout_keep_prob: float [0,1]. Probability of dropping something out
			max_norm: boolean. Whether to clip weights to 1-norm
			batch_norm: boolean. Whether to do batch normalization
			dtype: type of the weigths. Usually tf.float32
			idx: integer. Number of layer (for naming/scoping)
		Returns
		  	y: the batch after it leaves the block
		"""
		with vs.variable_scope("two_linear_" + str(idx)) as scope:
			input_size = int(xin.get_shape()[1])
			# Linear 1
			w2 = tf.get_variable(name="w2_" + str(idx), initializer=kaiming, shape=[input_size, linear_size],
								 dtype=dtype, trainable=True)
			b2 = tf.get_variable(name="b2_" + str(idx), initializer=kaiming, shape=[linear_size], dtype=dtype,
								 trainable=True)
			w2 = tf.clip_by_norm(w2, 1) if max_norm else w2
			y = tf.matmul(xin, w2) + b2
			if batch_norm:
				y = tf.layers.batch_normalization(y, training=self.isTraining, name="batch_normalization1" + str(idx))
			y = tf.nn.relu(y)
			y = tf.nn.dropout(y, dropout_keep_prob)

			# Linear 2
			w3 = tf.get_variable(name="w3_" + str(idx), initializer=kaiming, shape=[linear_size, linear_size],
								 dtype=dtype, trainable=True)
			b3 = tf.get_variable(name="b3_" + str(idx), initializer=kaiming, shape=[linear_size], dtype=dtype,
								 trainable=True)
			w3 = tf.clip_by_norm(w3, 1) if max_norm else w3
			y = tf.matmul(y, w3) + b3

			if batch_norm:
				y = tf.layers.batch_normalization(y, training=self.isTraining, name="batch_normalization2" + str(idx))
			y = tf.nn.relu(y)
			y = tf.nn.dropout(y, dropout_keep_prob)
			# Residual every 2 blocks
			y = (xin + y) if residual else y

		return y

	def add_grammar(self, pose3d, nodes=512):
		print('Adding grammar network upon base network')
		pose3d = RepeatVector(16)(pose3d)
		chain1 = Lambda(getRightLeg)(pose3d)
		chain2 = Lambda(getLeftLeg)(pose3d)
		chain3 = Lambda(getRightArm)(pose3d)
		chain4 = Lambda(getLeftArm)(pose3d)
		chain5 = Lambda(getHeadSpine)(pose3d)
		chain6 = Lambda(getUpperLimb)(pose3d)
		chain7 = Lambda(getLowerLimb)(pose3d)
		chain8 = Lambda(getLeftArmRightLeg)(pose3d)
		chain9 = Lambda(getRightArmLeftLeg)(pose3d)

		# Kinematics Grammar
		output1 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='kin_1'))(chain1)
		output1 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output1)
		output1 = Reshape((4 * 3,))(output1)
		output2 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='kin_2'))(chain2)
		output2 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output2)
		output2 = Reshape((4 * 3,))(output2)
		output3 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='kin_3'))(chain3)
		output3 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output3)
		output3 = Reshape((4 * 3,))(output3)
		output4 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='kin_4'))(chain4)
		output4 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output4)
		output4 = Reshape((4 * 3,))(output4)
		output5 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='kin_5'))(chain5)
		output5 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output5)
		output5 = Reshape((4 * 3,))(output5)

		# Symmetry Grammar
		output6 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='sym_1'))(chain6)
		output6 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output6)
		output6 = Reshape((7 * 3,))(output6)
		output7 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='sym_2'))(chain7)
		output7 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output7)
		output7 = Reshape((7 * 3,))(output7)

		# Coordination Grammar
		output8 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='crd_1'))(chain8)
		output8 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output8)
		output8 = Reshape((8 * 3,))(output8)
		output9 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='crd_2'))(chain9)
		output9 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output9)
		output9 = Reshape((8 * 3,))(output9)

		# merge_output = Concatenate(axis=-1)([output1, output2, output3, output4, output5, output6, output7, output8, output9])
		merge_output = PoseJointMerge()([output1, output2, output3, output4, output5, output6, output7, output8, output9])

		# # Symmetry Grammar
		# output6 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='sym_1'))(chain6)
		# output6 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output6)
		# output6 = Reshape((7 * 3,))(output6)
		# output7 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='sym_2'))(chain7)
		# output7 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output7)
		# output7 = Reshape((7 * 3,))(output7)
		#
		# # Coordination Grammar
		# output8 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='crd_1'))(chain8)
		# output8 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output8)
		# output8 = Reshape((8 * 3,))(output8)
		# output9 = Bidirectional(SimpleRNN(nodes, return_sequences=True, activation=None, name='crd_2'))(chain9)
		# output9 = TimeDistributed(Dense(3, kernel_initializer=my_init))(output9)
		# output9 = Reshape((8 * 3,))(output9)
		#
		# # merge_output = Concatenate(axis=-1)([output1, output2, output3, output4, output5, output6, output7, output8, output9])
		# merge_output = PoseJointMerge()([output1, output2, output3, output4, output5, output6, output7, output8, output9])

		return merge_output

	def step(self, session, encoder_inputs, decoder_outputs, decoder_outputs_joint, dropout_keep_prob, isTraining=True, isValid=False):
		"""Run a step of the model feeding the given inputs.
		Args
			session: tensorflow session to use
			encoder_inputs: list of numpy vectors to feed as encoder inputs
			decoder_outputs: list of numpy vectors that are the expected decoder outputs
			dropout_keep_prob: (0,1] dropout keep probability
			isTraining: whether to do the backward step or only forward
		Returns
			if isTraining is True, a 4-tuple
			loss: the computed loss of this batch
			loss_summary: tf summary of this batch loss, to log on tensorboard
			learning_rate_summary: tf summary of learnign rate to log on tensorboard
			outputs: predicted 3d poses
			if isTraining is False, a 3-tuple
			(loss, loss_summary, outputs) same as above
		"""
		input_feed = {self.encoder_inputs: encoder_inputs,
					  self.decoder_outputs: decoder_outputs,
					  self.decoder_outputs_joint: decoder_outputs_joint,
					  self.isTraining: isTraining,
					  self.dropout_keep_prob: dropout_keep_prob}

		# Output feed: depends on whether we do a backward step or not.
		if isTraining:
			output_feed = [self.updates,  # Update Op that does SGD
						   self.loss,
						   self.loss_summary,
						   self.learning_rate_summary,
						   self.outputs,
						   self.outputs_joint]

			outputs = session.run(output_feed, input_feed)
			return outputs[1], outputs[2], outputs[3], outputs[4], outputs[5]
		elif isValid:
			output_feed = [self.loss,
						   self.loss_summary,
						   self.outputs,
						   self.outputs_joint]

			outputs = session.run(output_feed, input_feed)
			return outputs[0], outputs[1], outputs[2], outputs[3]
		else:
			output_feed = [self.outputs,
						   self.outputs_joint]

			outputs = session.run(output_feed, input_feed)
			return outputs[0], outputs[1]  # No gradient norm

	def get_all_batches(self, data_x, data_y, camera_frame, data_mean=None, data_std=None, dim_to_use=None, add_noise=False, training=True, data_y_joint=None):
		"""
		Obtain a list of all the batches, randomly permutted
		Args
			data_x: dictionary with 2d inputs
			data_y: dictionary with 3d expected outputs
			camera_frame: whether the 3d data is in camera coordinates
			training: True if this is a training batch. False otherwise.
		Returns
			encoder_inputs: list of 2d batches
			decoder_outputs: list of 3d batches
		"""
		if add_noise:
			data_x = copy.deepcopy(data_x)
			data_x = data_utils.normalize_data(data_x, data_mean, data_std, dim_to_use, add_noise)

		# Figure out how many frames we have
		n = 0
		for key2d in data_x.keys():
			n2d, _ = data_x[key2d].shape
			n = n + n2d

		encoder_inputs = np.zeros((n, self.input_size), dtype=float)
		decoder_outputs = np.zeros((n, self.output_size), dtype=float)
		decoder_outputs_joint = np.zeros((n, self.output_size), dtype=float)

		# Put all the data into big arrays
		idx = 0
		for key2d in data_x.keys():
			(subj, b, fname) = key2d
			# keys should be the same if 3d is in camera coordinates
			key3d = key2d if (camera_frame) else (subj, b, '{0}.h5'.format(fname.split('.')[0]))
			key3d = (subj, b, fname[:-3]) if fname.endswith('-sh') and camera_frame else key3d

			n2d, _ = data_x[key2d].shape
			encoder_inputs[idx:idx + n2d, :] = data_x[key2d]
			decoder_outputs[idx:idx + n2d, :] = data_y[key3d]
			if training:
				decoder_outputs_joint[idx:idx + n2d, :] = data_y_joint[key3d]
			idx = idx + n2d

		if training:
			# Randomly permute everything
			idx = np.random.permutation(n)
			encoder_inputs = encoder_inputs[idx, :]
			decoder_outputs = decoder_outputs[idx, :]
			decoder_outputs_joint = decoder_outputs_joint[idx, :]

		# Make the number of examples a multiple of the batch size
		n_extra = n % self.batch_size
		if n_extra > 0:  # Otherwise examples are already a multiple of batch size
			encoder_inputs = encoder_inputs[:-n_extra, :]
			decoder_outputs = decoder_outputs[:-n_extra, :]
			if training:
				decoder_outputs_joint = decoder_outputs_joint[:-n_extra, :]

		n_batches = n // self.batch_size
		encoder_inputs = np.split(encoder_inputs, n_batches)
		decoder_outputs = np.split(decoder_outputs, n_batches)
		if training:
			decoder_outputs_joint = np.split(decoder_outputs_joint, n_batches)

		if training:
			if add_noise:
				self.queue_encoder_inputs = encoder_inputs
				self.queue_decoder_outputs = decoder_outputs
				self.queue_decoder_outputs_joint = decoder_outputs_joint
			else:
				return encoder_inputs, decoder_outputs, decoder_outputs_joint
		else:
			return encoder_inputs, decoder_outputs

	def fetch_result(self):
		return self.queue_encoder_inputs, self.queue_decoder_outputs, self.queue_decoder_outputs_joint
