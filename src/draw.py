import numpy as np
from sklearn.cluster import MiniBatchKMeans
import cameras
import data_utils
import linear_model
import copy
import matplotlib.pyplot as plt
import time
from sklearn.externals import joblib
from sklearn import mixture,metrics
import matplotlib.pyplot
import matplotlib.mlab
import os
from matplotlib.colors import LogNorm
import numpy.distutils.system_info as sysinfo
sysinfo.get_info('atlas')

from colour import Color


def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
    return A1*matplotlib.mlab.normpdf(x,mu1,sigma1) + A2*matplotlib.mlab.normpdf(x,mu2,sigma2)


def fit_mixture(data1, data2, centroid1, centroid2, joint, axis, cam='', ncomp=2, doplot=False):
    # clf = mixture.BayesianGaussianMixture(n_components=ncomp, covariance_type='full',max_iter=4000,tol=1e-9, n_init=2)
    # clf.fit(data)
    # if not os.path.exists('data/h36m/statistics-ft/atomicPose'+str(centroid)):
    #   os.makedirs('data/h36m/statistics-ft/atomicPose'+str(centroid))
    # joblib.dump(clf, 'data/h36m/statistics-ft/atomicPose'+str(centroid)+'/joint'+str(joint)+axis+cam+'.pkl') 
    if not os.path.exists('data/h36m/statistics-ft/pic/'):
      os.makedirs('data/h36m/statistics-ft/pic/'+'joint')
    # file1 = open('data/h36m/statistics-ft/pic/'+'1joint'+str(joint)+'.txt','w')
    # file2 = open('data/h36m/statistics-ft/pic/'+'2joint'+str(joint)+'.txt','w')
    clf = joblib.load('data/h36m/'+'/statistics-ft/atomicPose'+str(centroid1)+'/joint'+str(joint)+'x'+cam+'.pkl') 
    ml = clf.means_
    wl = clf.weights_
    cl = clf.covariances_
    ms = [m[0] for m in ml]
    cs = [np.sqrt(c[0][0]) for c in cl]
    ws = [w for w in wl]
    clf2 = joblib.load('data/h36m/'+'/statistics-ft/atomicPose'+str(centroid1)+'/joint'+str(joint)+'y'+cam+'.pkl') 
    ml2 = clf2.means_
    wl2 = clf2.weights_
    cl2 = clf2.covariances_
    ms2 = [m2[0] for m2 in ml2]
    cs2 = [np.sqrt(c2[0][0]) for c2 in cl2]
    ws2 = [w2 for w2 in wl2]
    clf3 = joblib.load('data/h36m/'+'/statistics-ft/atomicPose'+str(centroid2)+'/joint'+str(joint)+'x'+cam+'.pkl') 
    ml3 = clf3.means_
    wl3 = clf3.weights_
    cl3 = clf3.covariances_
    ms3 = [m3[0] for m3 in ml3]
    cs3 = [np.sqrt(c3[0][0]) for c3 in cl3]
    ws3 = [w3 for w3 in wl3]
    clf4 = joblib.load('data/h36m/'+'/statistics-ft/atomicPose'+str(centroid2)+'/joint'+str(joint)+'y'+cam+'.pkl') 
    ml4 = clf4.means_
    wl4 = clf4.weights_
    cl4 = clf4.covariances_
    ms4 = [m4[0] for m4 in ml4]
    cs4 = [np.sqrt(c4[0][0]) for c4 in cl4]
    ws4 = [w4 for w4 in wl4]
    y=0
    y2=0
    if doplot == True:
        plt.gcf().clear()
        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        x = np.linspace(-10., 10.)
        y = np.linspace(-10.,10.)
        X, Y = np.meshgrid(x, y)
        Z1 = np.zeros((len(x),len(x)))
        Z2 = np.zeros((len(x),len(x)))
        for i in range(len(x)):
            for j in range(len(y)):
                Z1[i][j] = clf.score_samples(x[i])*clf2.score_samples(y[j])
                Z2[i][j] = clf3.score_samples(x[i])*clf4.score_samples(y[j])
            #     file1.write(Z1[i][j])
            #     file1.write('\t')
            #     file2.write(Z2[i][j])
            #     file2.write('\t')
            # file1.write('\n')
            # file2.write('\n')
        # file1.close()
        # file2.close()
        # histo = matplotlib.pyplot.hist(data1, 60, normed=True, stacked=True,color=['burlywood'])
        # histo2 = matplotlib.pyplot.hist(data2, 60, normed=True, stacked=True,color=['chartreuse'])
        # for w, m, c in zip(ws, ms, cs):
        #     y =y+w*matplotlib.mlab.normpdf(histo[1],m,c)
        # for w2, m2, c2 in zip(ws2, ms2, cs2):
        #     y2 =y2+w2*matplotlib.mlab.normpdf(histo2[1],m2,c2)
        # plt.plot(histo[1],y, linewidth=3,c='orange')
        # plt.plot(histo2[1],y2, linewidth=3,c='green').
        blue = Color("#01579B")
        colors = list(blue.range_to(Color("#E0F7FA"),25))
        color_list = []
        for c in colors:
            color_list.append(c.rgb)
        red = Color("#E65100")
        colors = list(red.range_to(Color("#FFF8E1"),25))
        color_list2 = []
        for c in colors:
            color_list2.append(c.rgb)
        CS1 = plt.contour(X, Y, Z1,  colors=tuple(color_list),
                         levels=np.logspace(0.6, 1, 25))
        CS2 = plt.contour(X, Y, Z2,  colors=tuple(color_list2),
                         levels=np.logspace(0.6, 1 , 25))
        #CB = plt.colorbar(CS, shrink=0.8, extend='both')
        #plt.scatter(X_train[:, 0], X_train[:, 1], .8)
        
        # plt.plot(histo[1],y, linewidth=3,c='orange')
        # plt.plot(histo2[1],y2, linewidth=3,c='green')

        plt.draw()
        #plt.savefig('data/h36m/statistics-ft/atomicPose'+str(centroid)+'/joint'+str(joint)+axis+cam+'.png')

        plt.savefig('data/h36m/statistics-ft/pic/'+'joint'+str(joint)+'.pdf',format='pdf')
    return ms, cs, ws
# if True, atomic pose and GMM curve will be saved
Visualize = False
camera = [0,1,2,3]
cameraNames = ''

#Prepare data
actions = data_utils.define_actions( 'All' )
SUBJECT_IDS = [1,5,6,7,8,9,11]
rcams = cameras.load_cameras("data/h36m/cameras.h5", SUBJECT_IDS)
# Load 2d data
train_set_gt = data_utils.load_data( 'data/h36m/', [1,5,6,7,8], actions, dim=3 )
train_set_gt = data_utils.project_to_cameras( train_set_gt, rcams, ncams=camera )
#load SH
train_set_dt = data_utils.load_stacked_hourglass( 'data/h36m/', [1,5,6,7,8], actions)
# Compute normalization statistics.
complete_train = copy.deepcopy( np.vstack( train_set_gt.values() ))
data_mean, data_std, dim_to_ignore, dim_to_use = data_utils.normalization_stats( complete_train, dim=2 )
# move gt pose to root to compute atomic pose
train_set_gt_aligned = data_utils.postprocess_2d( train_set_gt,dim_to_use )

n = 0
for key2d in train_set_gt.keys():
  n2d, _ = train_set_gt[ key2d ].shape
  n = n + n2d

encoder_inputs_gt  = np.zeros((n, 32), dtype=float)
encoder_inputs_dt  = np.zeros((n, 32), dtype=float)
encoder_inputs_gt_aligned  = np.zeros((n, 32), dtype=float)
# Put all the data into big arrays
idx = 0
for key2d in train_set_gt.keys():
  (subj, b, fname) = key2d

  n2d, _ = train_set_gt[ key2d ].shape
  encoder_inputs_gt[idx:idx+n2d, :]  = train_set_gt[ key2d ][:, dim_to_use]
  encoder_inputs_dt[idx:idx+n2d, :]  = train_set_dt[ (subj, b, fname+'-sh') ][:, dim_to_use]
  encoder_inputs_gt_aligned[idx:idx+n2d, :]  = train_set_gt_aligned[ key2d ]
  idx = idx + n2d

n_clusters = 42
# ====================== Uncomment to calculate atomic pose
# method = MiniBatchKMeans(n_clusters=n_clusters)
# y_pred = method.fit_predict(encoder_inputs_gt_aligned)
# print "Calinski-Harabasz Score n_clusters=", n_clusters,"score:", metrics.calinski_harabaz_score(encoder_inputs_gt_aligned, y_pred) 
# preds = method.cluster_centers_
#joblib.dump(method, 'data/h36m/K-Means.pkl') 

# ====================== if you want to re-calculate atomic pose, comment below
method = joblib.load('data/h36m/K-Means.pkl') 
y_pred = method.predict(encoder_inputs_gt_aligned)
preds = method.cluster_centers_

if(Visualize): #Vis or not
    #pair keyppoint ids
    pairRef = [[3,2], [2,1],[1,0],
            [0,4], [4,5], [5,6], 
            [0,8], [9,8],
            [8,10],[10,11],[11,12],
            [8,13],[14,15],[13,14] ];
    partColor = [1,1,1,1,2,2,0,0,0,0,3,3,3,1,4,4];
    Colors = ['m', 'r', 'b', 'r', 'b']
    if not os.path.exists('data/h36m/atomic-pose/'):
      os.makedirs('data/h36m/atomic-pose/')
    #draw predicted pose
    for i in xrange(len(preds)):
        plt.gcf().clear()
        plt.xlim(-250,250)
        plt.ylim(-250,250)
        pred = preds[i]
        for point_pair in pairRef:
            x1 = float(pred[2*(point_pair[0])]); y1 = -float(pred[2*(point_pair[0])+1])
            x2 = float(pred[2*(point_pair[1])]); y2 = -float(pred[2*(point_pair[1])+1])
            Color = Colors[partColor[point_pair[0]]]
            plt.plot([x1,x2], [y1,y2], color = Color, lw = 2)
        plt.draw()
        plt.savefig('data/h36m/atomic-pose/'+str(i)+'.png')
        print 'save atomic pose '+str(i)+' done'

if not os.path.exists('data/h36m/statistics-ft/'):
    os.makedirs('data/h36m/statistics-ft/')
print 'Start fitting GMM model'

for joint in range(16):
    print 'Fitting GaussianMixture model for atomic pose '+str(17)+', joint '+str(joint)
    if len(np.where(y_pred==17)[0]) > 0:
      data1 = encoder_inputs_gt[y_pred==33,2*joint:2*joint+1]-encoder_inputs_dt[y_pred==33,2*joint:2*joint+1]
      data2 = encoder_inputs_gt[y_pred==17,2*joint:2*joint+1]-encoder_inputs_dt[y_pred==17,2*joint:2*joint+1]
      fit_mixture(data1, data2, 33, 17, joint, 'x', cameraNames, ncomp=2, doplot=True)
      data1 = encoder_inputs_gt[y_pred==33,2*joint+1:2*joint+2]-encoder_inputs_dt[y_pred==33,2*joint+1:2*joint+2]
      data2 = encoder_inputs_gt[y_pred==17,2*joint+1:2*joint+2]-encoder_inputs_dt[y_pred==17,2*joint+1:2*joint+2]
      fit_mixture(data1, data2, 33, 17, joint, 'y', cameraNames, ncomp=2, doplot=True)
    print 'done'
