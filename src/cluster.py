import numpy as np
import cameras
import data_utils
import linear_model
import copy
import matplotlib.pyplot as plt
import time
from sklearn.externals import joblib
from sklearn import mixture,metrics
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot
import matplotlib.mlab
import os

import numpy.distutils.system_info as sysinfo
sysinfo.get_info('atlas')

def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
	return A1*matplotlib.mlab.normpdf(x,mu1,sigma1) + A2*matplotlib.mlab.normpdf(x,mu2,sigma2)


def fit_mixture(data, centroid, joint, axis, cam='', ncomp=2, doplot=False):
	# clf = mixture.BayesianGaussianMixture(n_components=ncomp, covariance_type='full',max_iter=4000,tol=1e-9, n_init=2)
	# clf.fit(data)
	# if not os.path.exists('data/h36m/statistics-ft/atomicPose'+str(centroid)):
	#	 os.makedirs('data/h36m/statistics-ft/atomicPose'+str(centroid))
	# joblib.dump(clf, 'data/h36m/statistics-ft/atomicPose'+str(centroid)+'/joint'+str(joint)+axis+cam+'.pkl')
	clf = joblib.load('data/h36m/'+'/statistics-ft/atomicPose'+str(centroid)+'/joint'+str(joint)+axis+cam+'.pkl')
	ml = clf.means_
	wl = clf.weights_
	cl = clf.covariances_
	ms = [m[0] for m in ml]
	cs = [np.sqrt(c[0][0]) for c in cl]
	ws = [w for w in wl]
	y = 0
	if doplot == True:
		plt.gcf().clear()
		plt.xlim(-20, 20)
		plt.ylim(0, 0.15)
		histo = matplotlib.pyplot.hist(data, 60, normed=True)
		for w, m, c in zip(ws, ms, cs):
			y =y+w*matplotlib.mlab.normpdf(histo[1],m,c)
		plt.plot(histo[1],y, linewidth=3)
		plt.draw()
		#plt.savefig('data/h36m/statistics-ft/atomicPose'+str(centroid)+'/joint'+str(joint)+axis+cam+'.png')
		if not os.path.exists('data/h36m/statistics-ft/pic/'+'joint'+str(joint)+axis):
			os.makedirs('data/h36m/statistics-ft/pic/'+'joint'+str(joint)+axis)
		plt.savefig('data/h36m/statistics-ft/pic/'+'joint'+str(joint)+axis+'/atomicPose'+str(centroid)+cam+'.png')
	return ms, cs, ws

# if True, atomic pose and GMM curve will be saved
Visualize = False
camera = [0,1,2,3]
cameraNames = ''

#Prepare data
actions = data_utils.define_actions( 'All' )
SUBJECT_IDS = [1,5,6,7,8,9,11]
rcams = cameras.load_cameras("data/h36m/cameras.h5", SUBJECT_IDS)
# Load 2d data
train_set_gt = data_utils.load_data( 'data/h36m/', [1,5,6,7,8], actions, dim=3 )
train_set_gt = data_utils.project_to_cameras( train_set_gt, rcams, ncams=camera )
#load SH
train_set_dt = data_utils.load_stacked_hourglass( 'data/h36m/', [1,5,6,7,8], actions)
# Compute normalization statistics.
complete_train = copy.deepcopy( np.vstack( train_set_gt.values() ))
data_mean, data_std, dim_to_ignore, dim_to_use = data_utils.normalization_stats( complete_train, dim=2 )
# move gt pose to root to compute atomic pose
train_set_gt_aligned = data_utils.postprocess_2d( train_set_gt,dim_to_use )

n = 0
for key2d in train_set_gt.keys():
	n2d, _ = train_set_gt[ key2d ].shape
	n = n + n2d

encoder_inputs_gt	= np.zeros((n, 32), dtype=float)
encoder_inputs_dt	= np.zeros((n, 32), dtype=float)
encoder_inputs_gt_aligned	= np.zeros((n, 32), dtype=float)
# Put all the data into big arrays
idx = 0
for key2d in train_set_gt.keys():
	(subj, b, fname) = key2d
	n2d, _ = train_set_gt[ key2d ].shape
	encoder_inputs_gt[idx:idx+n2d, :]	= train_set_gt[ key2d ][:, dim_to_use]
	encoder_inputs_dt[idx:idx+n2d, :]	= train_set_dt[ (subj, b, fname+'-sh') ][:, dim_to_use]
	encoder_inputs_gt_aligned[idx:idx+n2d, :]	= train_set_gt_aligned[ key2d ]
	idx = idx + n2d

n_clusters = 42
# ====================== Uncomment to calculate atomic pose
# method = MiniBatchKMeans(n_clusters=n_clusters)
# y_pred = method.fit_predict(encoder_inputs_gt_aligned)
# print "Calinski-Harabasz Score n_clusters=", n_clusters,"score:", metrics.calinski_harabaz_score(encoder_inputs_gt_aligned, y_pred) 
# preds = method.cluster_centers_
#joblib.dump(method, 'data/h36m/K-Means.pkl') 

# ====================== if you want to re-calculate atomic pose, comment below
method = joblib.load('data/h36m/K-Means.pkl') 
y_pred = method.predict(encoder_inputs_gt_aligned)
preds = method.cluster_centers_

if(Visualize): #Vis or not
	#pair keyppoint ids
	pairRef = [[3,2], [2,1],[1,0],[0,4],[4,5],[5,6],[0,8],[9,8],[8,10],[10,11],[11,12],[8,13],[14,15],[13,14]];
	partColor = [1,1,1,1,2,2,0,0,0,0,3,3,3,1,4,4];
	Colors = ['m', 'r', 'b', 'r', 'b']
	if not os.path.exists('data/h36m/atomic-pose/'):
		os.makedirs('data/h36m/atomic-pose/')
	#draw predicted pose
	for i in xrange(len(preds)):
		plt.gcf().clear()
		plt.xlim(-250,250)
		plt.ylim(-250,250)
		pred = preds[i]
		for point_pair in pairRef:
			x1 = float(pred[2*(point_pair[0])]); y1 = -float(pred[2*(point_pair[0])+1])
			x2 = float(pred[2*(point_pair[1])]); y2 = -float(pred[2*(point_pair[1])+1])
			Color = Colors[partColor[point_pair[0]]]
			plt.plot([x1,x2], [y1,y2], color = Color, lw = 2)
		plt.draw()
		plt.savefig('data/h36m/atomic-pose/'+str(i)+'.png')
		print 'save atomic pose '+str(i)+' done'

if not os.path.exists('data/h36m/statistics-ft/'):
		os.makedirs('data/h36m/statistics-ft/')
print 'Start fitting GMM model'
for centroid in range(n_clusters):
	for joint in range(16):
		print 'Fitting GaussianMixture model for atomic pose '+str(centroid)+', joint '+str(joint)
		if len(np.where(y_pred==centroid)[0]) > 0:
			data = encoder_inputs_gt[y_pred==centroid,2*joint:2*joint+1]-encoder_inputs_dt[y_pred==centroid,2*joint:2*joint+1]
			fit_mixture(data, centroid, joint, 'x', cameraNames, ncomp=2, doplot=True)
			data = encoder_inputs_gt[y_pred==centroid,2*joint+1:2*joint+2]-encoder_inputs_dt[y_pred==centroid,2*joint+1:2*joint+2]
			fit_mixture(data, centroid, joint, 'y', cameraNames, ncomp=2, doplot=True)
		print 'done'
